#include<stdio.h>
#include<string.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
    char data[20]={0};
    int fd=open("/proc/mytest_dir/test_proc",O_RDWR);

    strcpy(data,"hello kernel");
    int se=write(fd,data,strlen(data));
    if(se<0)
    {
        perror("write fail");
        return -1;
    }
    printf("write data:%s\n",data);

    lseek(fd,0, SEEK_SET);

    memset(data,0,sizeof(data));
    int re=read(fd,data,20);
    if(re<0)
    {
        perror("read fail");
        return -1;
    }
    printf("read data:%s\n",data);

    close(fd);
    return 0;
}
